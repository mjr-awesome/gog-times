#!/usr/bin/env bash
{
usage()
{
msg_help="Usage: ./"${app}" [option] [data]\n\n\nOptions:\n    -v                                   Verify Integrity\n    -d                                   Dir Only\n    -r [num]                             Max Depth\n    -f                                   File Only\n"
if command -v "7z" &>/dev/null; then
	msg_help+="    -t                                   Title Out\n"
fi
msg_help+="    -m [UTC]                             Manual Date\n    -q                                   Quiet Mode\n"
if command -v "attr" &>/dev/null; then
	msg_help+="    -x                                   enable Xattr\n"
fi
msg_help+="    -c                                   disable Colour\n    -h --help                            print Usage\n    -l --license                         print License\n    --                                   end options"
echo -e "\n${msg_help}\n"
exit 0
}
license()
{
echo -e "\nMIT License\n\nCopyright (c) 2019 mjr-awesome\n\nPermission is hereby granted, free of charge, to any person obtaining a copy\nof this software and associated documentation files (the \"Software\"), to deal\nin the Software without restriction, including without limitation the rights\nto use, copy, modify, merge, publish, distribute, sublicense, and/or sell\ncopies of the Software, and to permit persons to whom the Software is\nfurnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all\ncopies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\nSOFTWARE.\n"
exit 0
}
msg()
{
		local msgtitle
		if [[ "${data}" = "${app_name}" ]]; then
			msgtitle="${app_name}"
		elif [[ "${set_title}" = on ]]; then
			msgtitle="${data}"
		else
			msgtitle="${data_path}"
		fi
		msg_error()
		{
			if [[ "${data}" = "${app_name}" ]]; then
				echo -e "${msg_log}${msgtitle} ${clr_red_pop}[${clr_off}${clr_red}ERROR${clr_off}${clr_red_pop}]${clr_off}${clr_gray}:${clr_off} ${msgbody}" 1>&2
			else
				echo -e "${msg_log}${clr_red_pop}[${clr_off}${clr_red}ERROR${clr_off}${clr_red_pop}]${clr_off} ${msgbody}${clr_gray}:${clr_off} ${msgtitle}" 1>&2
			fi
		}
		msg_warning()
		{
			echo -e "${msg_log}${clr_orange_pop}[${clr_off}${clr_orange}WARN${clr_off}${clr_orange_pop}]${clr_off} ${msgbody}${clr_gray}:${clr_off} ${msgtitle}"
		}
		msg_information()
		{
			echo -e "${msg_log}${clr_spcl}[INFO]${clr_off} ${msgbody}${clr_gray}:${clr_off} ${msgtitle}"
		}
	local msgbody
	msgbody="${2}"
	if [[ "${set_colour}" = on ]]; then
		if [[ "${msgbody:0:9}" = "Not valid" || "${msgbody:0:10}" = "Can't find" ]]; then
			clr_red="\033[31;1;40m"
			clr_red_pop="\033[31;1;40m"
		else
			clr_red="\033[31;40m"
			clr_red_pop="\033[31;5;40m"
		fi
		clr_orange="\033[33;40m"
		clr_orange_pop="\033[33;5;40m"
		if [[ -z "${clr_spcl}" ]]; then
			clr_spcl="\033[34;1;40m"
		fi
		clr_gray="\033[0;2;40m"
		clr_off="\033[0m"
	else
		clr_red=""
		clr_orange=""
		clr_orange_pop=""
		clr_spcl=""
		clr_gray=""
		clr_off=""	
	fi
	if [[ -n "${2}" ]]; then
		if [[ "${1}" = e ]]; then
			if [[ "${set_quiet}" != "yes" ]]; then
				msg_error
			fi
			set_state=1
			return 1
		elif [[ "${1}" = w ]]; then
			if [[ "${set_quiet}" != "yes" ]]; then
				msg_warning
			fi
			set_state=1
			return 1
		elif [[ "${1}" = i ]]; then
			if [[ "${set_quiet}" != "yes" ]]; then
				msg_information
			fi
			set_state=0
			return 0
		elif [[ "${1}" = q ]]; then
			if [[ -n "${3}" ]]; then
				if [[ "${set_quiet}" != "yes" ]]; then
					if read -p "${app_name}: $( echo -e "${2}?> " )" ${3}; then
						return 0
					else
						return 1
					fi
				else
					return 1
				fi
			else
				return 1
			fi
		else
			return 1
		fi
	else
		return 1
	fi
}
set_cli()
{
	if [[ "${argc}" != 0 ]]; then
		local sco
		sco="${@}"
		if [[ "${sco:0:9}" = "--license" ]]; then
			license
		elif [[ "${sco:0:6}" = "--help" ]]; then
			usage
		fi
		while getopts ':sdr:ftcm:qvxhl' set_cli; do
			case "${set_cli}" in
				r) 
					set_depth="${OPTARG//[^0-9\-]}"
					;;
				d)
					set_type=dir
					;;
				f)
					set_type=file
					;;
				q)
					set_quiet=yes
					;;
				s)
					set_hidden=off
					;;
				t)
					set_title=on
					;;
				v)
					set_chcksm=on
					;;
				x)
					set_fsattr=on
					;;
				c)
					set_colour=off
					;;
				m)
					set_manual="${OPTARG}"
					;;
				l)
					license
					;;
				h)
					usage
					;;
				*)
					data="${app_name}"
					msg e "Incorrect Options Found"
					exit 1
			esac
		done
		shift "$(( "${OPTIND}" - 1 ))"
	else
		exit 0
	fi
	argc="${#}"
	argv=( "${@}" )
}
set_que()
{
	local stq_roll
	local stqetm
	stqetm="$(( $( ps x | grep -v grep | grep -c "${app}" ) / 20 ))"
	if [[ "${stqetm}" -ge 1 ]]; then 
		while ! ps x --sort=+start_time | grep -v grep | grep -m 5 "${app}" | grep "${app_id}" &>/dev/null; do
			sleep "${stqetm}"
		done
	fi
}
set_cmp()
{
	cmpflt()
	{
		sed 's/ \*\./|/g; s/\*\.//g; s/\./\\./g; s/^/\.\*\\\.(/g; s/$/)$/g'
	}
local requierements
local set_cmp_test
cmp_sevzip=no
cmp_imgmag=no
cmp_targzp=no
cmp_fsattr=no
cmp_ffmpeg=no
cmp_zenity=no
cmp_chcksm=no
data="${app_name}"
requierements=(
	basename
	cat
	chmod
	date
	dirname
	find
	file
	ls
	head
	ps
	sed
	sort
	readlink
	)
for set_cmp_test in "${requierements[@]}"; do
	if ! command -v "${set_cmp_test}" &>/dev/null; then
		msg e "${set_cmp_test} isn't installed"
		exit 1
	fi
done
if [[ "${set_fsattr}" = on ]]; then
	if command -v "attr" &>/dev/null; then
		cmp_fsattr=yes
	else
		msg e "attr isn't installed"
		exit 1
	fi	
fi
if command -v "zenity" &>/dev/null; then
	cmp_zenity=yes
fi
if [[ "${set_chcksm}" = on ]]; then
	if command -v "osslsigncode" &>/dev/null; then
		cmp_chcksm=yes
		return 0
	else
		msg e "osslsigncode isn't installed"
		exit 1
	fi
fi
if [[ "${set_chcksm}" = off ]]; then
	flt_others=(
		"*.exe"
		"*.msi"
		"*.pdf"
		"*.sh"
		"*.torrent"
		)
	rgx_others="$( echo "${flt_others[@]}" | cmpflt )"
	filetypes+=( ${flt_others[@]} )
		flt_sevzip=(
				"*.7z"
				"*.deb"
				"*.dmg"
				"*.iso"
				"*.pkg"
				"*.rpm"
				"*.rar"
				"*.zip"
				)
		rgx_sevzip="$( echo "${flt_sevzip[@]}" | cmpflt )"
	if command -v "7z" &>/dev/null; then
		cmp_sevzip=yes
		filetypes+=( ${flt_sevzip[@]} )
	fi
		flt_imgmag=(
				"*.jpeg"
				"*.jpg"
				"*.png"
				)
		rgx_imgmag="$( echo "${flt_imgmag[@]}" | cmpflt )"
	if command -v "identify" &>/dev/null; then
		cmp_imgmag=yes
		filetypes+=( ${flt_imgmag[@]} )
	fi
		flt_targzp=(
				"*.tar"
				"*.tar.bz2"
				"*.tb2"
				"*.tbz2"
				"*.tbz"
				"*.tar.gz"
				"*.tgz"
				"*.tar.xz"
				"*.txz"
				)
		rgx_targzp="$( echo "${flt_targzp[@]}" | cmpflt )"
	if command -v "tar" &>/dev/null; then
		cmp_targzp=yes
		filetypes+=( ${flt_targzp[@]} )
	fi
		rgx_ffmpeg=(
				"*.mp4"
				)
		rgx_ffmpeg="$( echo "${rgx_ffmpeg[@]}" | cmpflt )"
	if command -v "ffmpeg" &>/dev/null; then
		cmp_ffmpeg=yes
		filetypes+=( ${rgx_ffmpeg[@]} )
	fi
	filetypes=( $( echo -e "$(echo "${filetypes[@]}" | sed 's/ /\\n/g; s/\*/*/g')" | sort ) )
	rgx_files="$( echo "${filetypes[@]}" | cmpflt )"
fi
}
set_act()
{
	go_dir()
	{
		if ! [[ -f "${app_path}" ]]; then
			msg e "No ${app} found"
			exit 1		
		elif ! [[ -x "${app_path}" ]]; then
			chmod +x "${app_path}"
		fi
		if [[ "${set_quiet}" = yes ]]; then
			optfind+=q
		fi
		if [[ "${set_title}" = on ]]; then
			optfind+=t
		fi
		if [[ "${set_fsattr}" = on ]]; then
			optfind+=x
		fi
		if [[ "${set_colour}" = off ]]; then
			optfind+=c
		fi
		if [[ -n "${set_manual}" ]]; then
			optfind+="m ${set_manual}"
		fi
		if [[ -z "${set_depth}" ]]; then
			if find -L "${data_path}" -type f -regextype posix-egrep -iregex "${rgx_files}" -execdir "${app_path}" "-s${optfind}" -- '{}' \; 2>/dev/null; then
				return 0
			else
				set_state=1
				return 1
			fi
		elif [[ "${set_depth}" -le 0 ]]; then
			return 1
		else
			if find -L "${data_path}" -maxdepth "${set_depth}" -type f -regextype posix-egrep -iregex "${rgx_files}" -execdir "${app_path}" "-s${optfind}" -- '{}' \; 2>/dev/null; then
				return 0
			else
				set_state=1
				return 1
			fi
		fi
	}
	go_fls()
	{
		prepare_bin()
		{
			prepare_done="yes"
			if [[ "${data_type}" = "octet-stream" ]] || [[ "${data_type}" = "x-rar" ]]; then
				if [[ -f "${data_dir}/${data_bin_exe}" ]]; then
					data="${data_bin_exe}"
					data_type="x-dosexec"
					data_path="${data_dir}/${data}"
					if go_fls; then
						appstate=0
					else
						appstate=1
					fi
				else
					msg e "No exe was found"
					return 1
				fi
			fi
		}
		prepare_others()
		{
				if [[ "${data_type}" = "x-dosexec" ]]; then
					prepare_done="yes"
					prpthr()
					{
						grep -E '\^E1\^O\^W\^M.*#\^F' | sed 's/.*\^E1\^O\^W\^M/20/; s/Z0#\^F.*//' | sed -r "s/[-\:\ ]//g; s/.{4}/&-/;  s/.{7}/&-/; s/.{10}/&T/; s/.{13}/&:/; s/.{16}/&:/;  s/.{19}/&+00:00/" | sort -r | head -n 1 | grep "^....-..-..T..:..:..[+\-]..:..$"
					}
					prpthr_data()
					{
						if [[ "${cmp_sevzip}" = "yes" ]]; then
							local rawexec
							local data_name
							rawexec="$( 7z l -- "${data_path}" 2>/dev/null | sed "s/[\$\`\\']//g; s/\"/\'/g; s/[^[:print:]]//g" )"
							data_name="$( echo "${rawexec}" | grep -m 1 "ProductName: " | sed -e 's/[[:space:]]*$//; s/^ProductName: //; s/\[/(/g; s/\]/)/g' | grep -m 1 . )"
							if [[ -n "${data_name}" ]]; then
								data="$( echo "\"${data_name}\"" )"
							else
								if [[ "${data}" = "setup_toca_3.exe" ]]; then
									data="\"Toca Race Driver 3\""
								else
									return 1
								fi
							fi
						fi
					}
					local data_exe_xcptns
					local exn
					local exv
					data_exe_xcptns=(
						[20150109051501]="setup_wizardry7dos_2.0.0.11.exe"
						[20150109064117]="setup_revolt_2.1.0.5.exe"
						[20150303154346]="setup_colin2005.exe"
						[20150306134627]="setup_toca_3.exe"
						[20150114125642]="setup_full_spectrum_warrior_2.1.0.7.exe"
						[20180516094417]="setup_defcon_1.6_(20793).exe"
					)
					go_fls_date=""
					for exn in "${!data_exe_xcptns[@]}"; do
						for exv in "${data_exe_xcptns[$exn]}"; do
							if [[ "${exv}" = "${data}" ]]; then
								go_fls_date="$( echo "${exn}" | sed -r 's/$/+00:00/; s/.{12}/&:/; s/.{4}/&-/; s/.{7}/&-/; s/.{10}/&T/; s/.{13}/&:/' | grep "^....-..-..T..:..:..[+\-]..:..$" )"
								break 2
							fi
						done
					done
					if [[ -z "${go_fls_date}" ]] && command -v tac &>/dev/null; then
						go_fls_date="$( tac -- "${data_path}" 2>/dev/null  | cat -e | head -n 255 | prpthr )"
					fi
					if [[ -z "${go_fls_date}" ]]; then
						go_fls_date="$( 7z l -- "${data_path}" 2>/dev/null | tail -n 1 )"
						go_fls_date="$( date -u -d "${go_fls_date/  *}" +%Y-%m-%dT%H:%M:%S+00:00 )"
						prepare_done="yes"
						if [[ "${go_fls_date:0:2}" = 19 ]]; then
							go_fls_date=""
						fi
					fi
					if [[ "${set_title}" = on ]]; then
						prpthr_data
					fi
				elif [[ "${data_type}" = "x-msi" ]]; then
					prepare_done="yes"
					data_type="x-dosexec"
					prepare_others
					if [[ -z "${go_fls_date}" ]] && [[ "${cmp_sevzip}" = "yes" ]]; then
						data_type="zip"
						prepare_sevzip
					fi
				elif [[ "${data_type}" = "pdf" ]]; then
						altthr()
						{
							local pdf_alt
							local pdf_plus
							local pdf_tp
							pdf_alt=(
								CreateDate
								ModifyDate
								MetadataDate
								)
							pdf_plus=(
								xap
								xmp
								)
							for pdf_nm in "${pdf_alt[@]}"; do
								for pdf_tp in "${pdf_plus[@]}"; do
									if [[ -z "${go_fls_date}" ]]; then
										go_fls_date="$( cat -e -- "${data_path}" 2>/dev/null | grep -m 1 "${pdf_tp}:${pdf_nm}>" | sed -r "s/<\/${pdf_tp}.*//g; s/\'//g; s/.*${pdf_tp}:${pdf_nm}>//g; s/${pdf_tp}:.*//g; s/Z$/+00:00/g" | grep "^....-..-..T..:..:.." )"
									else
										break 2
									fi
								done
							done
					}
					prepare_done="yes"
					local pdf_main
					local pdf_nm
					pdf_main=(
						CreationDate
						ModDate
						)
					go_fls_date=""
					for pdf_nm in "${pdf_main[@]}"; do
						if [[ -z "${go_fls_date}" ]]; then
							go_fls_date="$( cat -e -- "${data_path}" 2>/dev/null  | grep -m 1 "/${pdf_nm}" | sed -r "s/Date\(/Date (/g; s/.*\/${pdf_nm} \(D://g; s/\).*//g; s/.{4}/&-/; s/.{7}/&-/; s/.{10}/& /; s/.{13}/&:/; s/.{16}/&:/; s/.00.$/:00/g; s/ /T/g; s/Z$/+00:00/g; s/'$//g; s/'/:/g" | grep "^....-..-..T..:..:.." )"
						else
							break
						fi
					done
					if [[ -z "${go_fls_date}" ]]; then
						altthr
					fi
					if [[ -n "${go_fls_date}" ]]; then
						if ! echo "${go_fls_date}" | grep "^....-..-..T..:..:..[+\-]..:..$" &>/dev/null; then
							go_fls_date="$(echo "${go_fls_date}" | sed 's/$/+00:00/g' | grep "^....-..-..T..:..:..[+\-]..:..$" )"
						fi
					fi
				elif [[ "${data_type}" = "x-shellscript" ]]; then
					prpthr_data()
					{
						local data_name
						data_name="$( echo "${rawshell}" | grep -m 1 ^label=\' | sed "s/^label='//; s/ (GOG.com)//; s/'$//" )"
						if [[ -n "${data_name}" ]]; then
							data="$( echo "\"${data_name}\"" )"
						else
							return 1
						fi
					}
					local rawshell
					local checktype
					local checkgog
					rawshell="$( cat -e -- "${data_path}" 2>/dev/null | head -n 500 | sed "s/[\$\`\\']//g; s/\"/\'/g; s/[^[:print:]]//g" | grep -v "^#" )"
					checkgog="$( echo "${rawshell}" | grep -c GOG )"
					checktype="$( echo "${rawshell}" | grep -c -E ^CRCsum=\|^MD5=\|^label=\|Makeself )"
					if [[ "${checkgog}" -ge 2 ]] && [[ "${checktype}" -ge 5 ]]; then
						prepare_done="yes"
						go_fls_date="$( echo "${rawshell}" | grep -m 1 "echo Date of packaging: .* .* .* ..:..:.. [A-Z].*[A-Z] 2...$" | sed 's/echo Date of packaging: //; s/\$//; s/[^a-zA-Z0-9 :]//g' )"
						if [[ "${set_title}" = on ]]; then
							prpthr_data
						fi
					else
						msg e "Not valid data"
						return 1
					fi
				elif [[ "${data_type}" = "x-bittorrent" ]]; then
					prepare_done="yes"
					go_fls_date="$( date -d "@$( head -n 1 "${data_path}" | cat -e | grep -m 1 ":creation datei.*:info" | sed 's/.*:creation datei//; s/e4:info.*//;' )" +%Y-%m-%dT%H:%M:%S%+00:00 )"
				fi
		}
		prepare_sevzip()
		{
			altszp()
			{
				if [[ -n "${1}" ]]; then
					go_fls_date="$( 7z l -p- -- "${data_path}" 2>/dev/null | grep -m 1 "^${1} = " | sed "s/^${1} = //" | sed -n -e "s/\(^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\) \([0-9][0-9]:[0-9][0-9]:[0-9][0-9]\)/\1T\2+00:00/p" | sort -r | head -n 1 | grep "^....-..-..T..:..:..[+\-]..:..$" )"
				else
					return 1
				fi
			}
			prpszp()
			{
				prpnox()
				{
					go_fls_date="$( 7z l -p- -- "${data_path}" 2>/dev/null | grep -v " D\\.\\.\\.\\. " | head --lines=-2 | sed -n -e "s/\(^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\) \([0-9][0-9]:[0-9][0-9]:[0-9][0-9]\).*/\1T\2+00:00/p" | sort -r | head -n 1 | grep "^....-..-..T..:..:..[+\-]..:..$" )"
				}
				
				if echo "${data}" | grep \\.dmg$ &>/dev/null; then
					local data_dmg_xcptns
					local exn
					local exv
					data_dmg_xcptns=(
						[20150114161223]="strike_suit_zero_all_dlc_1.0.0.6.dmg"
						[20151118142206]="stronghold_crusader_classic_1.0.0.5.dmg"
						[20151203161514]="stronghold_classic_1.0.0.12.dmg"
					)
					go_fls_date=""
					for exn in "${!data_dmg_xcptns[@]}"; do
						for exv in "${data_dmg_xcptns[$exn]}"; do
							if [[ "${exv}" = "${data}" ]]; then
								go_fls_date="$( echo "${exn}" | sed -r 's/$/+00:00/; s/.{12}/&:/; s/.{4}/&-/; s/.{7}/&-/; s/.{10}/&T/; s/.{13}/&:/' | grep "^....-..-..T..:..:..[+\-]..:..$" )"
								break 2
							fi
						done
					done
					if [[ -z "${go_fls_date}" ]]; then
						prpnox
					fi
				elif echo "${data}" | grep \\.zip &>/dev/null; then
					if echo "${data}" | grep -E "^Outcast_Making_Of\\.zip$|^Outcast_Outtakes\\.zip$" &>/dev/null; then
						return 1
					else
						prpnox
					fi
				else
					prpnox
				fi
			}
			prepare_done="yes"
			if [[ "${data_type}" = "x-7z-compressed" ]]; then
				prpszp
			elif [[ "${data_type}" = "vnd.debian.binary-package" ]]; then
				altszp "Modified"
			elif [[ "${data_type}" = "zlib" ]]; then
				prpszp
			elif [[ "${data_type}" = "x-iso9660-image" ]] || [[ "${data_type}" = "octet-stream" ]]; then
				prpszp
			elif [[ "${data_type}" = "x-xar" ]]; then
				prpszp
			elif [[ "${data_type}" = "x-rpm" ]]; then
				altszp "Created"
			elif [[ "${data_type}" = "x-rar" ]]; then
				prpszp
			elif [[ "${data_type}" = "zip" ]]; then		
				prpszp
			fi
		}
		prepare_imgmag()
		{
			altimg(){
					if [[ -n "${1}" ]]; then
					go_fls_date="$( identify -verbose -- "${data_path}" 2>/dev/null | grep -m 1 "${1}" | sed "s/    ${1} //g; s/:/-/1; s/:/-/1; s/$/+00:00/g; s/ /T/g" | grep "^....-..-..T..:..:..[+\-]..:..$" )"
				else
					return 1
				fi
			}
			prepare_done="yes"
			if [[ "${data_type}" = "jpeg" ]]; then
				altimg "exif:DateTimeOriginal:"
				if [[ -z "${go_fls_date}" ]]; then
					altimg "exif:DateTime:"
				fi
			elif [[ "${data_type}" = "png" ]]; then
				altimg "png:tIME:"
				if [[ -z "${go_fls_date}" ]]; then
					altimg "Creation Time:"
				fi
			fi
		}
		prepare_tararc()
		{
			prptrc()
			{
				go_fls_date="$( tar -t -v --full-time -f "${data_path}" 2>/dev/null | sed -n -e 's/^.* \([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] .[0-9]\:[0-9][0-9]\:[0-9][0-9]\).*/\1/p' | sed -r 's/[-\:\ ]//g; s/.{4}/&-/; s/.{7}/&-/; s/.{10}/&T/; s/.{13}/&:/; s/.{16}/&:/; s/.{19}/&+00:00/' | sort -r | head -n 1 | grep "^....-..-..T..:..:..[+\-]..:..$" )"
			}
				prepare_done="yes"
				if [[ "${data_type}" = "x-bzip2" ]]; then
					prptrc
				elif [[ "${data_type}" = "gzip" ]]; then
					prptrc
				elif [[ "${data_type}" = "x-tar" ]]; then
					prptrc
				elif [[ "${data_type}" = "x-xz" ]]; then
					prptrc
				fi
		}
		prepare_ffmpeg()
		{
			prpfmg()
			{
				go_fls_date="$( ffprobe -v quiet -print_format json -show_format -- "${data_path}" 2>/dev/null | grep -m 1 "\"creation_time\":" | sed 's/.*"creation_time": .//; s/\..*/+00:00/' | grep "^....-..-..T..:..:..[+\-]..:..$" )"
			}
			prepare_done="yes"
			if [[ "${data_type}" = "mp4" ]]; then
				prpfmg
			fi
		}
		apply_prepared()
		{
			time_apply() {
				if [[ "${data_type}" != "x-dosexec" ]]; then
					touch -c -m -t "${time_stamp}" -- "${data_dir}/${data}" &>/dev/null
				else
					set +o noglob
					set -o braceexpand
					data_exe="$( echo "${data}" | sed 's/\.exe$//' )"
					touch -c -m -t "${time_stamp}" -- "${data_dir}/${data_exe}"{.exe,.bin,-[0-9].bin,-[0-9][0-9].bin,-[0-9][0-9][0-9].bin,.EXE,.BIN,-[0-9].BIN,-[0-9][0-9].BIN,-[0-9][0-9][0-9].BIN} &>/dev/null
					set -o noglob
					set +o braceexpand
				fi
				if [[ "${set_fsattr}" = on ]]; then
					if [[ "${cmp_fsattr}" = yes ]]; then
						setfattr -n user.gogtimes -v "$( gflstm )" "${data_path}" &>/dev/null
					fi
				fi
			}
			local time_stamp
			local data_first
			if [[ -n "${go_fls_date}" ]]; then
				time_stamp="$( date -d "${go_fls_date}" +%Y%m%d%H%M.%S 2>/dev/null | grep ............\\... )"
			else
				msg w "Can't get date"
				return 1
			fi
			if time_apply; then
				go_fls_time_after="$( gflstm )"
				if [[ "${go_fls_time_before}" != "${go_fls_time_after}" ]]; then
					clr_spcl="\033[32;40m"
					msg i "Updated time stamp for"
				else
					return 0
				fi
			fi
		}
		data_type="$( file --mime-type -b -- "${data_path}" )"
		data_type="$( echo "${data_type}" | sed 's/^cannot open .* \(No such file or directory\)//; s/.*\///g; s/[^a-zA-Z0-9\.\-]//g' )"
			gflstm()
			{
				ls --full-time -- "${data_path}" | sed -n -e "s/.* [0-9].* [[:print:]].* [[:print:]].* [0-9].*[0-9] \(2...-..-..\) \(..:..:..\)\.......... \([+\-]..\)\(..\) .*/\1T\2\3:\4/p"
			}
		go_fls_time_before="$( gflstm )"
		if [[ "${set_fsattr}" = on ]]; then
			if [[ "${cmp_fsattr}" = yes ]]; then
				go_fls_time_fsattr="$(echo -n "$( getfattr -n user.gogtimes --only-values "${data_path}" 2>/dev/null )" )"
				if [[ "${go_fls_time_before}" = "${go_fls_time_fsattr}" ]]; then
					clr_spcl="\033[34;1;40m"
					msg i "Nothing was updated on"
					return 0
				fi
			fi
		fi
		prepare_done="no"
		go_fls_date=""
		if [[ "${set_hidden}" = "off" ]] && echo "${data}" | grep -E ^\\..\|^\\.$\|^\\.\\.$ &>/dev/null; then
			return 1
		fi
		if [[ -n "${set_manual}" ]]; then
			prepare_done=yes
			go_fls_date="$( date -u -d "${set_manual}" +%Y-%m-%dT%H:%M:%S+00:00 2>/dev/null )"
			if [[ -z "${go_fls_date}" ]]; then
				msg e "Incorrect Manual Date"
				exit 1
			fi
		elif [[ -n "${go_fls_time_fsattr}" ]]; then
			prepare_done=yes
			go_fls_date="${go_fls_time_fsattr}"
		else
			if echo "${data}" | grep \\.bin &>/dev/null; then
				data_bin_exe=$( echo "${data}" | sed 's/)\-[1-9].*\.bin$/).exe/g; s/\-[1-9].*\.bin$/.exe/g; s/\.bin$/\.exe/g' )
				if echo "${argv[@]}" | grep -F "${data_bin_exe}" &>/dev/null; then
					return 0
				else
					if prepare_bin; then
						return 0
					else
						return 1
					fi
				fi
			fi
			if echo "${data}" | grep -Ei "${rgx_others}" &>/dev/null; then
				prepare_others
			elif [[ "${cmp_sevzip}" = yes ]] && echo "${data}" | grep -Ei "${rgx_sevzip}"\|"\\.zip\\.[0-9][0-9][0-9]$" &>/dev/null; then
				prepare_sevzip
			elif [[ "${cmp_imgmag}" = yes ]] && echo "${data}" | grep -Ei "${rgx_imgmag}" &>/dev/null; then
				prepare_imgmag
			elif [[ "${cmp_targzp}" = yes ]] && echo "${data}" | grep -Ei "${rgx_targzp}" &>/dev/null; then
				prepare_tararc
			elif [[ "${cmp_ffmpeg}" = yes ]] && echo "${data}" | grep -Ei "${rgx_ffmpeg}" &>/dev/null; then
				prepare_ffmpeg
			else
				if echo "${data}" | grep -Ei "${rgx_sevzip}"\|"\\.zip\\.[0-9][0-9][0-9]$" &>/dev/null; then
					msg e "7z isn't installed"
				elif echo "${data}" | grep -Ei "${rgx_imgmag}" &>/dev/null; then
					msg e "imagemagick isn't installed"
				elif echo "${data}" | grep -Ei "${rgx_targzp}" &>/dev/null; then
					msg e "tar isn't installed"
				elif echo "${data}" | grep -Ei "${rgx_ffmpeg}" &>/dev/null; then
					msg e "ffmpeg isn't installed"
				else
					msg e "Not valid data"
					return 1
				fi
			fi
		fi
		if [[ "${prepare_done}" = "yes" ]]; then
			if apply_prepared; then
				return 0
			else
				return 1
			fi
		else
			return 1
		fi
	}
local data_date
	stctxd()
	{
		echo -n "$( getfattr -n user.gogtimes --only-values "${set_manual}" 2>/dev/null | grep "^....-..-..T..:..:..[+\-]..:..$" )"
	}
	stctfd()
	{
		ls --full-time -- "${set_manual}" 2>/dev/null | sed -n -e "s/.* [0-9].* [[:print:]].* [[:print:]].* [0-9].*[0-9] \(2...-..-..\) \(..:..:..\)\.......... \([+\-]..\)\(..\) .*/\1T\2\3:\4/p" | grep "^....-..-..T..:..:..[+\-]..:..$"
	}
if [[ "${argc}" = 1 ]] && [[ -f "${set_manual}" ]] && [[ "${set_fsattr}" = on ]] && [[ -f "${argv[0]}" ]]; then
	data_date=$( stctxd )
	if [[ -n "${data_date}" ]]; then
		if touch -c -m -t "$(date -d "${data_date}" "+%Y%m%d%H%M.%S" )" -- "${argv[0]}"; then
			exit 0
		else
			exit 1
		fi
	else
		exit 1
	fi
elif [[ "${argc}" = 1 ]] && [[ -f "${set_manual}" ]] && [[ -f "${argv[0]}" ]]; then
	data_date=$( stctfd )
	if [[ -n "${data_date}" ]]; then
		if touch -c -m -t "$(date -d "${data_date}" "+%Y%m%d%H%M.%S" )" -- "${argv[0]}"; then
			exit 0
		else
			exit 1
		fi
	else
		exit 1
	fi
elif [[ "${argc}" = 0 ]] && [[ -f "${set_manual}" ]] && [[ "${set_fsattr}" = on ]]; then
	data_date=$( stctxd )
	if [[ -n "${data_date}" ]]; then
		if echo "touch -c -m -t \"$(date -d "${data_date}" "+%Y%m%d%H%M.%S" )\" -- \"${set_manual}\""; then
			exit 0
		else
			exit 1
		fi
	else
		exit 1
	fi
elif [[ "${argc}" = 0 ]] && [[ -f "${set_manual}" ]]; then
	data_date=$( ls --full-time -- "${set_manual}" 2>/dev/null | sed -n -e "s/.* [0-9].* [[:print:]].* [[:print:]].* [0-9].*[0-9] \(2...-..-..\) \(..:..:..\)\.......... \([+\-]..\)\(..\) .*/\1T\2\3:\4/p" | grep "^....-..-..T..:..:..[+\-]..:..$" )
	if [[ -n "${data_date}" ]]; then
		if echo "touch -c -m -t \"$(date -d "${data_date}" "+%Y%m%d%H%M.%S" )\" -- \"${set_manual}\""; then
			exit 0
		else
			exit 1
		fi
	else
		exit 1
	fi
elif [[ "${argc}" = 0 ]] && [[ -n "${set_manual}" ]]; then
	if echo "touch -c -m -t \"$(date -d "${set_manual}" "+%Y%m%d%H%M.%S" )\" -- [data]"; then
		exit 0
	else
		exit 1
	fi
elif [[ "${argc}" = 0 ]] && [[ "${cmp_zenity}" = yes ]]; then
	local zenout
	zenout="$( echo "${filetypes[@]}" )"
	zenout="$( zenity --file-selection --multiple --title="${app_name}" --separator="${srt}" --file-filter="All Files | ${zenout}" | sed "s/${srt}/\n/g" )"
	zenout="${zenout//['$`']}"
	if [[ -n "${zenout}" ]]; then
		readarray -t argv < <( echo "${zenout}" )
	else
		exit 0
	fi
elif [[ "${argc}" = 0 ]]; then
	usage
fi
		chcksm_out()
		{
		gogsum_c()
		{
			local i binnum binsum exe md5gog md5max binargv
			gogsum_s()
			{
				if [[ -z "${dat}" ]]; then
					exe="${1}"
				fi
				if [[ -z "${1}" ]]; then
					return 1
				fi
				i="$(( ${#1} - 2 ))"
				binnum="${1:$i:2}"
				binsum="${1%%"${binnum}"}"
				if [[ "${i:0:1}" = 0 ]]; then
					binnum="${binnum/0}"
				fi
				binnow="$( ls -v "${dat%%\.exe}"*.bin 2>/dev/null | grep -c '^' )"
				binnum=( $( seq 0 "$(( "${binnum}" - 1 ))" ) )
				for i in "${binnum[@]}"; do
					bintmp="${binsum:0:32}"
					binsum="${binsum/"${binsum:0:32}"}"
					if [[ -f "${dat%%\.exe}.bin" ]]; then
						echo "${bintmp}  ${dat%%\.exe}.bin"
					else
						echo "${bintmp}  ${dat%%\.exe}-$(( "${i}" + 1 )).bin"
					fi
				done
			}
			gogsum_r()
			{
				if [[ -z "${dat}" ]]; then
					exe="${1}"
				fi
				md5gog="$( tac -- "${dat}" 2>/dev/null | cat -e | grep '#GOGCRCSTRING' )"
				md5gog="${md5gog/'#GOGCRCSTRING'*}"
			    if [[ "${dat##*\/}" = "setup_batman_the_enemy_within_-_the_telltale_series_episode_5_(19607).exe" ]]; then
			    	    	md5gog=bee5fee915e988ad198eff71db98d35c724b5c5b80b0b45d241cee92fb0d4b8cae5af3da8a31100f66c010b1aba626a50188ae461c0cb07e3bb2d5e90389fb1789433eb7afc4c1c7819f2098e9a3a674aa63e9cd880209bc62a4f4e70477570806
	    	elif [[ "${dat##*\/}" = "setup_bloodrayne_2_1.0_(19403).exe" ]]; then
	    		md5gog=896d1e41bca56cb9bc9587f151f7572901
	    	elif [[ "${dat##*\/}" = "setup_broken_sword_3_-_the_sleeping_dragon_1.0_(19115).exe" ]]; then
	    		md5gog=3901d740c9071b43eae5b5e3566ef4c601
	    	elif [[ "${dat##*\/}" = "setup_colin2005.exe" ]]; then
	    		md5gog=3931964c5eb08e8e09eb22c5860a4f9f9c00ae95ab1ebc512fce9c32b5d7bab402
	    	elif [[ "${dat##*\/}" = "setup_expeditions_viking_1.0.7.4_(19111).exe" ]]; then
	    		md5gog=1a62dce3a32d0d596bd708a259771a12b2791ee5edaa1c5ed93762d638ddec3902
	    	elif [[ "${dat##*\/}" = "setup_little_nightmares_1.0.43.1_(18471).exe" ]]; then
	    		md5gog=0732121bf0da7db6dc6f5e5a3bc8705b01
		   	elif [[ "${dat##*\/}" = "setup_mafia_iii_1.090.0_(19569).exe" ]]; then	    	data=4754a97cfa22f7a4861961f48896b5f0aa5eaf4aef380c740caa81a78563141c086d4148409c1601387ef28b0935b9746c76acf1fef2943b909829f8785f8634f91d1aec2b776f55c3467305047dd01aa070c7c3fa3a5cdf6f40f02e577ac75fc15aad8d2e8d958c8744107fbc6bc99518de8744ef029e0512ccd06daafd255b3838974cd7f46cd4f400f5f54df509ea862034c31022d7d4908e4902ad6f6bb71af264bc5d0eac6eef59e14f9e9df61c59ccb31ef701f48d772406b7823492139a7e9db959199ef24ae5adef7810ab5213
	    	elif [[ "${dat##*\/}" = "setup_mark_of_the_ninja_1.0_(19120).exe" ]]; then
	    		md5gog=a6e0f1035d5e2f938b3f9cad9f4f973d01
	    	elif [[ "${dat##*\/}" = "setup_sniper_elite_-_berlin_1945_1.0_(19500).exe" ]]; then
	    		md5gog=3bcafdcd6d92a3ede2a8c73df72b6c3901
	    	elif [[ "${dat##*\/}" = "setup_the_raven_remastered_1.1.0.654_(19262).exe" ]]; then
	    		md5gog=d12388215b70f2ccc9b033e8e323d39201d315ba1f8808f7fa2f6210cdb8d17ad361cf5faea20ec79562d1321568e9c603
	    	elif [[ "${dat##*\/}" = "setup_the_witness_21-12-2017_(19091).exe" ]]; then
	    		md5gog=90a24fed5d8e2e52ab1bf2de8434998401
	    	elif [[ "${dat##*\/}" = "setup_toca_3.exe" ]]; then
	    		md5gog=4187245c9d32a7ddd930f26fc75dc58d4b3b4235d439a2409dbb108981abc147d6a5a69f5f6a5506adaea2c80835b18303
	    	elif [[ "${dat##*\/}" = "setup_ufo_aftermath_1.4_(19448).exe" ]]; then
	    		md5gog=53eefd9b1e788cb1fdb068b0f34e911501
	    	elif [[ "${dat##*\/}" = "setup_ufo_aftershock_1.2_(19449).exe" ]]; then
	    		md5gog=9401fc55950a8b8e5726e669163f765201
	    	fi
			if echo "${md5gog}" | grep '00$' &>/dev/null; then
				md5gog=""
				return 1
			else
				md5max="$(( "${#md5gog}" - 2 ))"
				md5num="${md5gog:$md5max:2}"
				if [[ -z "${md5num}" ]]; then
					return 0
				fi
				md5gog="${md5gog%%"${md5num}"}"
				md5gog="$( echo "${md5gog}" | rev )"
				md5max="$(( "${md5num}" * 32 ))"
				md5gog="$( echo "${md5gog:0:md5max}" | rev )"
				echo "${md5gog}${md5num}"
			fi
		}
		dat="${1}"
		if [[ -n "${dat}" ]]; then
			gtitle="${dat##*\.}"
	    	gtitle="${gtitle,,}"
		    if [[ "${gtitle}" = "zip" ]]; then
				if command -v "unzip" &>/dev/null; then
			        unzip -t "${dat}" &>/dev/null
			        if [[ "${?}" = 0 ]] || [[ "${?}" = 1 ]]; then
						if zip -F "${dat}" -O /dev/null | grep '^Zip entry offsets do not need adjusting$' &>/dev/null; then
							echo "${dat}: OK"
						else
							echo "${dat}: FAILED"
						fi
			        else
			            echo "${dat}: FAILED"
			        fi
				else
					echo "${dat}: UNKNOWN"
				fi
			elif [[ "${gtitle}" = "exe" ]]; then
				readarray -t binargv < <( ls -v "${dat%%\.exe}"*.bin 2>/dev/null )
				if command -v "osslsigncode" &>/dev/null; then
					if osslsigncode verify "${dat}" &>/dev/null; then
						echo "${dat}: OK"
					else
						echo "${dat}: FAILED"
					fi
				else
					echo "${dat}: UNKNOWN"
				fi
				if [[ "$( echo "${dat%%\.exe}"*.bin )" != "${dat%%\.exe}*.bin" ]]; then
					gogmd5="$( gogsum_r "${dat}" )"
					if [[ -n "${gogmd5}" ]]; then
						gogsum_s "${gogmd5}" | md5sum -c
					else
						for i in "${binargv[@]}"; do
							echo "${i}: UNKNOWN"
						done
					fi
				fi
			else
				return 1
			fi
		fi
		}
		for duck in "${@}"; do
			if [[ "${duck:0:7}" = "file://" ]]; then
				duck="${duck:7}"
			fi
			if [[ -d "${duck}" ]]; then
				readarray -t argvc < <( find -L "${duck}" -type f -regextype posix-egrep -iregex ".*\.(bin|exe|zip)$" 2>/dev/null )
				for i in "${argvc[@]}"; do
					gogsum_c "${i}"
				done
			else
				gogsum_c "${duck}"
			fi
		done
	}
if [[ "${cmp_chcksm}" = yes ]]; then
	set +o noglob
	set -o braceexpand
	if chcksm_out "${argv[@]}"; then
		return 0
	else
		return 1
	fi
fi
for data_path in "${argv[@]}"; do
	if [[ "${data_path:0:7}" = "file://" ]]; then
		data_path="${data_path:7}"
	fi
	data_path="$( readlink -m -- "${data_path}" 2>/dev/null )"
	data="$( basename -- "${data_path}" 2>/dev/null )"
	if [[ -e "${data_path}" ]]; then
		if [[ -r "${data_path}" ]]; then
			if [[ "${set_type}" = dir ]]; then
				if [[ -d "${data_path}" ]]; then
					data_dir="${data_path}"
					go_dir
				elif [[ -f "${data_path}" ]]; then
					msg e "Not a directory"
					continue
				fi
			elif  [[ "${set_type}" = file ]]; then
				if [[ -d "${data_path}" ]]; then
					msg e "Not a file"
					continue
				elif [[ -f "${data_path}" ]]; then
					data_dir="$( dirname -- "${data_path}" 2>/dev/null )"
					go_fls
				fi
			else
				if [[ -d "${data_path}" ]]; then
					data_dir="${data_path}"
					go_dir
				elif [[ -f "${data_path}" ]]; then
					data_dir="$( dirname -- "${data_path}" 2>/dev/null )"
					go_fls
				fi
			fi
		else
			msg e "Can't read data"
			continue
		fi
	else
		msg e "Can't find data"
		continue
	fi
done
}
main()
{
set -o noglob
set +o braceexpand
set +o histexpand
set -o noclobber
app_name="GOG Times"
app_id="${BASHPID}"
echo "${app_id}" &>/dev/null
app_path="${0//['$`']}"
app_path="$( readlink -m -- "${app_path//[^[:print:]]}" 2>/dev/null )"
app="$( basename -- "${app_path}" 2>/dev/null )"
app_dir="$( dirname -- "${app_path}" 2>/dev/null )"
data="${1//[^[:print:]]}"
set_state=0
set_quiet=no
set_hidden=on
set_title=off
set_fsattr=off
set_colour=on
set_chcksm=off
set_type=""
set_manual=""
set_filter=""
srt="Ü¢åÄÏ·ÿÎÀ®Æ¨üêØª¥¿ÆÞÕÙçë°î¿äÈ§±»"
set_cli "${@//[^[:print:]]}"
set_cmp
set_que
set_act
if [[ "${set_state}" = 0 ]]; then
	exit 0
else
	exit "${set_state}"
fi
}

main "${@//['$`']}"
exit 1
}
