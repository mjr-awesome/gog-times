>  # GOG Times

Usage: ./gog-times.sh [option] [data]

**Options:**

    -v                                   Verify Integrity
    -d                                   Dir Only
    -r [num]                             Max Depth
    -f                                   File Only
    -t                                   Title Out
    -m [UTC]                             Manual Date
    -q                                   Quiet Mode
    -x                                   enable Xattr
    -h --help                            print Usage
    -l --license                         print License
    --                                   end options
